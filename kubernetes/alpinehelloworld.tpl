---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: REPO
  name: REPO
spec:
  replicas: 2
  selector:
    matchLabels:
      app: REPO
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: REPO
    spec:
      containers:
      - image: registry.gitlab.com/USER/REPO:master
        name: REPO
        Env:  #c l’expression de notre deployment
        - name: PORT
          value: "5000"
      imagePullSecrets: #car il fo recuperer l’image ki stocké juste au #dessus de lui
      - name: regcred
 
---
apiVersion: v1
kind: Service
metadata:
  name: REPO
spec:
  type: NodePort #ici c pr exposer notre service kon veut de type node #port
  ports:
    - name: REPO
      nodePort: 32000
      port: 5000  #ici c le port de notre application kon a fournit en #terme de variable d'environnement
      targetPort: 5000
      protocol: TCP
  selector:
    app: 'REPO'
